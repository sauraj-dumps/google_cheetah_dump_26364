
�
android.app.admin.flags#device_policy_size_tracking_enabled
enterprise"DAdd feature to track the total policy size and have a max threshold.*	28154335108BE
?frameworks/base/core/java/android/app/admin/flags/flags.aconfigH 
�
android.app.admin.flagsonboarding_bugreport_v2_enabled
enterprise"aAdd feature to track required changes for enabled V2 of auto-capturing of onboarding bug reports.*	30251767708BE
?frameworks/base/core/java/android/app/admin/flags/flags.aconfigH 
�
android.app.admin.flags"policy_engine_migration_v2_enabled
enterprise"0V2 of the policy engine migrations for Android V*	28952069708BE
?frameworks/base/core/java/android/app/admin/flags/flags.aconfigH 
�
android.appapp_start_infosystem_performance"<Control collecting of ApplicationStartInfo records and APIs.*	24781485508BD
>frameworks/base/core/java/android/app/activity_manager.aconfigH 
�
android.appsystem_terms_of_address_enabledgrammatical_gender"(Feature flag for System Terms of Address*	29779886608BR
Lframeworks/base/core/java/android/app/grammatical_inflection_manager.aconfigH 
�
android.app.usagereport_usage_stats_permissionbackstage_power"7Feature flag for the new REPORT_USAGE_STATS permission.*	29605677108B?
9frameworks/base/core/java/android/app/usage/flags.aconfigH 
�
android.app.usageuser_interaction_type_apibackstage_power"8Feature flag for user interaction event report/query API*	29606123208B?
9frameworks/base/core/java/android/app/usage/flags.aconfigH 
�
android.companioncompanion_transport_apis	companion".Grants access to the companion transport apis.*	28829750508B?
9frameworks/base/core/java/android/companion/flags.aconfigH 
�
android.companionnew_association_builder	companion"4Controls if the new Builder is exposed to test apis.*	29625148108B?
9frameworks/base/core/java/android/companion/flags.aconfigH 
�
android.companion.virtual.flagsdynamic_policyvirtual_devices"Enable dynamic policy API*	29840178008BG
Aframeworks/base/core/java/android/companion/virtual/flags.aconfigH 
�
android.companion.virtual.flagsenable_native_vdmvirtual_devices"Enable native VDM service*	30353537608BG
Aframeworks/base/core/java/android/companion/virtual/flags.aconfigH
�
android.companion.virtual.flags	more_logsvirtual_devices"More logs to test flags with*	29172582308BG
Aframeworks/base/core/java/android/companion/virtual/flags.aconfigH 
�
android.companion.virtual.flagsstream_permissionsvirtual_devices"6Enable streaming permission dialogs to Virtual Devices*	29173791908BG
Aframeworks/base/core/java/android/companion/virtual/flags.aconfigH 
�
android.companion.virtual.flagsvdm_custom_homevirtual_devices"Enable custom home API*	29716832808BG
Aframeworks/base/core/java/android/companion/virtual/flags.aconfigH 
�
android.companion.virtual.flagsvdm_public_apisvirtual_devices"-Enable public VDM API for device capabilities*	29725352608BG
Aframeworks/base/core/java/android/companion/virtual/flags.aconfigH 
�
android.companion.virtual.flagsvirtual_cameravirtual_devices"Enable Virtual Camera*	27035226408BG
Aframeworks/base/core/java/android/companion/virtual/flags.aconfigH 
�
android.content.pm	archivingpackage_manager_service"-Feature flag to enable the archiving feature.*	27855367008B@
:frameworks/base/core/java/android/content/pm/flags.aconfigH 
�
android.content.pmget_package_infopackage_manager_service"QFeature flag to enable the feature to retrieve package info without installation.*	26914927508B@
:frameworks/base/core/java/android/content/pm/flags.aconfigH 
�
android.content.pmimprove_install_freezepackage_manager_service",Feature flag to improve install freeze time.*	30756124208B@
:frameworks/base/core/java/android/content/pm/flags.aconfigH 
�
android.content.pmnullable_data_dirpackage_manager_service"9Feature flag to allow ApplicationInfo.dataDir to be null.*	30258781408B@
:frameworks/base/core/java/android/content/pm/flags.aconfigH
�
android.content.pmprevent_sdk_lib_apppackage_manager_service"AFeature flag to enable the prevent sdk-library be an application.*	29584361708B@
:frameworks/base/core/java/android/content/pm/flags.aconfigH
�
android.content.pmquarantined_enabledpackage_manager_service""Feature flag for Quarantined state*	26912743508B@
:frameworks/base/core/java/android/content/pm/flags.aconfigH 
�
android.content.pmsdk_lib_independencepackage_manager_service"\Feature flag to keep app working even if its declared sdk-library dependency is unavailable.*	29582795108B@
:frameworks/base/core/java/android/content/pm/flags.aconfigH
�
android.content.pmstay_stoppedbackstage_power"1Feature flag to improve stopped state enforcement*	29664491508B@
:frameworks/base/core/java/android/content/pm/flags.aconfigH 
�
android.content.pmuse_art_service_v2package_manager_service"tFeature flag to enable the features that rely on new ART Service APIs that are in the VIC version of the ART module.*	30474168508B@
:frameworks/base/core/java/android/content/pm/flags.aconfigH 
�
android.content.resdefault_localeresource_manager"/Feature flag for default locale in LocaleConfig*	11730640908BA
;frameworks/base/core/java/android/content/res/flags.aconfigH
�
android.credentials.flagsinstant_apps_enabledcredential_manager"4Enables Credential Manager to work with Instant Apps*	30219026908BA
;frameworks/base/core/java/android/credentials/flags.aconfigH 
�
android.credentials.flagssettings_activity_enabledcredential_manager"4Enable the Credential Manager Settings Activity APIs*	30001405908BA
;frameworks/base/core/java/android/credentials/flags.aconfigH 
�
android.database.sqlitesqlite_apis_15system_performance"$SQLite APIs held back for Android 15*	27904325308BE
?frameworks/base/core/java/android/database/sqlite/flags.aconfigH
�
android.hardware.biometricsadd_key_agreement_crypto_object
biometrics"9Feature flag for adding KeyAgreement api to CryptoObject.*	28205814608BI
Cframeworks/base/core/java/android/hardware/biometrics/flags.aconfigH 
�
android.hardware.radiohd_radio_improvedcar_framework"FFeature flag for improved HD radio support with less vendor extensions*	28030092908BD
>frameworks/base/core/java/android/hardware/radio/flags.aconfigH 
�
android.multiuser=bind_wallpaper_service_on_its_own_thread_during_a_user_switch	multiuser"fBind wallpaper service on its own thread instead of system_server's main handler during a user switch.*	30210034408BD
>frameworks/base/core/java/android/content/pm/multiuser.aconfigH 
�
android.multiuser5save_global_and_guest_restrictions_on_system_user_xml	multiuser"OSave guest and device policy global restrictions on the SYSTEM user's XML file.*	30106794408BD
>frameworks/base/core/java/android/content/pm/multiuser.aconfigH 
�
android.multiuser?save_global_and_guest_restrictions_on_system_user_xml_read_only	multiuser"`Save guest and device policy global restrictions on the SYSTEM user's XML file. (Read only flag)*	30106794408BD
>frameworks/base/core/java/android/content/pm/multiuser.aconfigH
�
android.multiusersupport_communal_profile	multiuser"'Framework support for communal profile.*	28542617908BD
>frameworks/base/core/java/android/content/pm/multiuser.aconfigH 
�
android.nfcenable_nfc_mainlinenfc"Flag for NFC mainline changes*	29214038708B9
3frameworks/base/core/java/android/nfc/flags.aconfigH 
�
android.nfcenable_nfc_reader_optionnfc"&Flag for NFC reader option API changes*	29118796008B9
3frameworks/base/core/java/android/nfc/flags.aconfigH 
�

android.osallow_private_profileprofile_experiences"cGuards a new Private Profile type in UserManager - everything from its setup to config to deletion.*	29906946008B8
2frameworks/base/core/java/android/os/flags.aconfigH 
�

android.os*disallow_cellular_null_ciphers_restrictioncellular_security"vGuards a new UserManager user restriction that admins can use to require cellular encryption on their managed devices.*	27675288108B8
2frameworks/base/core/java/android/os/flags.aconfigH 
�

android.os"remove_app_profiler_pss_collectionbackstage_power":Replaces background PSS collection in AppProfiler with RSS*	29754229208B8
2frameworks/base/core/java/android/os/flags.aconfigH 
�

android.osstate_of_health_publicsystem_sw_battery"5Feature flag for making state_of_health a public api.*	28884204508B8
2frameworks/base/core/java/android/os/flags.aconfigH 
�
android.os.vibrator#enable_vibration_serialization_apishaptics"=Enables the APIs for vibration serialization/deserialization.*	24512950908BA
;frameworks/base/core/java/android/os/vibrator/flags.aconfigH 
�
android.os.vibrator3haptic_feedback_vibration_oem_customization_enabledhaptics"@Enables OEMs/devices to customize vibrations for haptic feedback*	29112847908BA
;frameworks/base/core/java/android/os/vibrator/flags.aconfigH
�
android.os.vibratorhaptics_customization_enabledhaptics")Enables the haptics customization feature*	24191809808BA
;frameworks/base/core/java/android/os/vibrator/flags.aconfigH 
�
android.os.vibrator)haptics_customization_ringtone_v2_enabledhaptics"-Enables the usage of the new RingtoneV2 class*	24191809808BA
;frameworks/base/core/java/android/os/vibrator/flags.aconfigH 
�
android.os.vibratorkeyboard_category_enabledhaptics";Enables the independent keyboard vibration settings feature*	28910757908BA
;frameworks/base/core/java/android/os/vibrator/flags.aconfigH 
�
android.os.vibratoruse_vibrator_haptic_feedbackhaptics"nEnables performHapticFeedback to directly use the vibrator service instead of going through the window session*	29545908108BA
;frameworks/base/core/java/android/os/vibrator/flags.aconfigH 
�
android.permission.flagsdevice_aware_permission_apispermissions"#enable device aware permission APIs*	27485267008B@
:frameworks/base/core/java/android/permission/flags.aconfigH
�
android.permission.flags role_controller_in_system_serverpermissions"'enable role controller in system server*	30256259008B@
:frameworks/base/core/java/android/permission/flags.aconfigH 
�
android.permission.flagsset_next_attribution_sourcepermissions"1enable AttributionSource.setNextAttributionSource*	30447864808B@
:frameworks/base/core/java/android/permission/flags.aconfigH 
�
android.permission.flags"should_register_attribution_sourcepermissions".enable the shouldRegisterAttributionSource API*	30505769108B@
:frameworks/base/core/java/android/permission/flags.aconfigH 
�
android.permission.flags voice_activation_permission_apispermissions"'enable voice activation permission APIs*	28726430808B@
:frameworks/base/core/java/android/permission/flags.aconfigH 
�
android.securitydeprecate_fsv_sighardware_backed_security"%Feature flag for deprecating .fsv_sig*	27791618508B>
8frameworks/base/core/java/android/security/flags.aconfigH 
�
android.securityextend_vb_chain_to_updated_apkhardware_backed_security"YUse v4 signature and fs-verity to chain verification of allowlisted APKs to Verified Boot*	27791618508B>
8frameworks/base/core/java/android/security/flags.aconfigH
�
android.security!fix_unlocked_device_required_keyshardware_backed_security"<Fix bugs in behavior of UnlockedDeviceRequired keystore keys*	29646408308B>
8frameworks/base/core/java/android/security/flags.aconfigH 
�
android.securityfsverity_apihardware_backed_security"Feature flag for fs-verity API*	28518574708B>
8frameworks/base/core/java/android/security/flags.aconfigH 
�
android.service.autofillautofill_credman_integrationautofill">Guards Autofill Framework against Autofill-Credman integration*	29690728308B8
2frameworks/base/services/autofill/features.aconfigH 
�
android.service.autofill%fill_fields_from_current_session_onlyautofill"?Only fill autofill fields that are part of the current session.*	27072282508B8
2frameworks/base/services/autofill/bugfixes.aconfigH 
�
android.service.autofillrelayoutautofill"Mitigation for relayout issue*	29433042608B8
2frameworks/base/services/autofill/bugfixes.aconfigH 
�
android.service.autofilltestautofill"
Test flag *	29738004508B8
2frameworks/base/services/autofill/bugfixes.aconfigH 
�
android.service.voice.flags#allow_training_data_egress_from_hdsmachine_learning"`This flag allows the hotword detection service to egress training data to the default assistant.*	29607492408BI
Cframeworks/base/core/java/android/service/voice/flags/flags.aconfigH 
�
android.view.accessibilitya11y_overlay_callbacksaccessibility"NWhether to allow the passing of result callbacks when attaching a11y overlays.*	30447869108B\
Vframeworks/base/core/java/android/view/accessibility/flags/accessibility_flags.aconfigH 
�
android.view.accessibility$allow_shortcut_chooser_on_lockscreenaccessibility"DAllows the a11y shortcut disambig dialog to appear on the lockscreen*	30387172508B\
Vframeworks/base/core/java/android/view/accessibility/flags/accessibility_flags.aconfigH 
�
android.view.accessibilityforce_invert_coloraccessibility"EEnable force force-dark for smart inversion and dark theme everywhere*	28282164308B\
Vframeworks/base/core/java/android/view/accessibility/flags/accessibility_flags.aconfigH 
�
$android.view.contentprotection.flagsblocklist_update_enabledcontent_protection"DIf true, content protection blocklist is mutable and can be updated.*	30165800808Be
_frameworks/base/core/java/android/view/contentprotection/flags/content_protection_flags.aconfigH 
�
$android.view.contentprotection.flagsparse_groups_config_enabledcontent_protection"9If true, content protection groups config will be parsed.*	30218792208Be
_frameworks/base/core/java/android/view/contentprotection/flags/content_protection_flags.aconfigH 
�
$android.view.contentprotection.flagssetting_ui_enabledcontent_protection"oIf true, content protection setting ui is displayed in Settings > Privacy & Security > More security & privacy.*	30579234808Be
_frameworks/base/core/java/android/view/contentprotection/flags/content_protection_flags.aconfigH 
�
android.view.flagsexpected_presentation_time_apitoolkit"FFeature flag for using expected presentation time of the Choreographer*	27873019708BM
Gframeworks/base/core/java/android/view/flags/refresh_rate_flags.aconfigH 
�
android.view.flagsscroll_feedback_apitoolkit"Enable the scroll feedback APIs*	23959427108BP
Jframeworks/base/core/java/android/view/flags/scroll_feedback_flags.aconfigH 
�
android.view.flagsset_frame_rate_callbackcore_graphics""Enable the `setFrameRate` callback*	29994622008BM
Gframeworks/base/core/java/android/view/flags/refresh_rate_flags.aconfigH 
�
android.view.flagstoolkit_set_frame_ratetoolkit"*Feature flag for toolkit to set frame rate*	29351296208BM
Gframeworks/base/core/java/android/view/flags/refresh_rate_flags.aconfigH 
�
android.view.flags,use_view_based_rotary_encoder_scroll_hapticstoolkit"�If enabled, the rotary encoder scroll haptic implementation in the View class will be used, and the HapticScrollFeedbackProvider logic for rotary encoder haptic will be muted.*	29958701108BP
Jframeworks/base/core/java/android/view/flags/scroll_feedback_flags.aconfigH 
�
android.view.flagsview_velocity_apitoolkit"*Feature flag for view content velocity api*	29351381608BM
Gframeworks/base/core/java/android/view/flags/refresh_rate_flags.aconfigH 
�
android.view.flagswm_display_refresh_rate_testcore_graphics":Adds WindowManager display refresh rate fields to test API*	30447519908BM
Gframeworks/base/core/java/android/view/flags/refresh_rate_flags.aconfigH 
�
android.view.inputmethodeditorinfo_handwriting_enabledinput_method"<Feature flag for adding EditorInfo#mStylusHandwritingEnabled*	29389818708BF
@frameworks/base/core/java/android/view/inputmethod/flags.aconfigH
�
android.view.inputmethodimm_userhandle_hostsidetestsinput_method"QFeature flag for replacing UserIdInt with UserHandle in some helper IMM functions*	30171330908BF
@frameworks/base/core/java/android/view/inputmethod/flags.aconfigH
�
android.view.inputmethodrefactor_insets_controllerinput_method"RFeature flag for refactoring InsetsController and removing ImeInsetsSourceConsumer*	29817224608BF
@frameworks/base/core/java/android/view/inputmethod/flags.aconfigH
�
android.widget.flags0enable_platform_widget_differential_motion_flingtoolkit"5Enables differential motion fling in platform widgets*	29333208908B\
Vframeworks/base/core/java/android/widget/flags/differential_motion_fling_flags.aconfigH 
�
com.android.graphics.flagsexact_compute_boundsframework_graphics"<Add a function without unused exact param for computeBounds.*	30447855108BF
@frameworks/base/graphics/java/android/framework_graphics.aconfigH 
�
com.android.graphics.hwui.flagsgainmap_animationscore_graphics"1APIs to help enable animations involving gainmaps*	29648228908B:
4frameworks/base/libs/hwui/aconfig/hwui_flags.aconfigH 
�
com.android.graphics.hwui.flags!gainmap_constructor_with_metadatacore_graphics"8APIs to create a new gainmap with a bitmap for metadata.*	30447855108B:
4frameworks/base/libs/hwui/aconfig/hwui_flags.aconfigH 
�
com.android.graphics.hwui.flagshdr_10bit_pluscore_graphics"7Use 10101010 and FP16 formats for HDR-UI when available*	28415948808B:
4frameworks/base/libs/hwui/aconfig/hwui_flags.aconfigH 
�
com.android.graphics.hwui.flagslimited_hdrcore_graphics"FAPI to enable apps to restrict the amount of HDR headroom that is used*	23418196008B:
4frameworks/base/libs/hwui/aconfig/hwui_flags.aconfigH 
�
com.android.hardware.inputkeyboard_a11y_sticky_keys_flaginput_native"`Controls if the sticky keys accessibility feature for physical keyboard is available to the user*	29454633508BN
Hframeworks/base/core/java/android/hardware/input/input_framework.aconfigH 
�
com.android.hardware.inputkeyboard_layout_preview_flaginput_native"^Controls whether a preview will be shown in Settings when selecting a physical keyboard layout*	29357937508BN
Hframeworks/base/core/java/android/hardware/input/input_framework.aconfigH 
�
com.android.intentresolverexample_new_sharing_methodintentresolver"*Enables the example new sharing mechanism.*<none>08BB
<packages/modules/IntentResolver/aconfig/FeatureFlags.aconfigH 
�
com.android.intentresolvermodular_frameworkintentresolver"!Enables the new modular framework*	30211351908BB
<packages/modules/IntentResolver/aconfig/FeatureFlags.aconfigH 
�
com.android.intentresolverscrollable_previewintentresolver"/Makes preview scrollable with multiple profiles*	28710290408BB
<packages/modules/IntentResolver/aconfig/FeatureFlags.aconfigH 
�
com.android.intentresolvertarget_data_cachingintentresolver"5Enables caching target icons and labels in a local DB*	28531484408BB
<packages/modules/IntentResolver/aconfig/FeatureFlags.aconfigH 
�
!com.android.internal.camera.flagscamera_hsum_permissioncamera_platform"%Camera access by headless system user*	27353963108B2
,frameworks/av/camera/camera_platform.aconfigH 
�
!com.android.internal.camera.flags$camera_manual_flash_strength_controlcamera_platform"3Flash brightness level control in manual flash mode*	23834888108B2
,frameworks/av/camera/camera_platform.aconfigH 
�
!com.android.internal.camera.flagslazy_aidl_wait_for_servicecamera_platform"<Use waitForService instead of getService with lazy AIDL HALs*	28554620808B2
,frameworks/av/camera/camera_platform.aconfigH 
�
!com.android.internal.camera.flagslog_ultrawide_usagecamera_platform"DEnable measuring how much usage there is for ultrawide-angle cameras*	30051579608B2
,frameworks/av/camera/camera_platform.aconfigH 
�
$com.android.internal.foldables.flagsfold_lock_setting_enableddisplay_manager""Feature flag for Fold Lock Setting*	27444776708B^
Xframeworks/base/core/java/com/android/internal/foldables/fold_lock_setting_flags.aconfigH
�
$com.android.internal.telephony.flagscarrier_enabled_satellite_flag	telephony"AThis flag controls satellite communication supported by carriers.*	29643738808B6
0frameworks/opt/telephony/flags/satellite.aconfigH 
�
$com.android.internal.telephony.flags.conference_hold_unhold_changed_to_send_message	telephony"QThis flag controls Conferences hold & unHold operation changed to send a message*	28800298908B0
*frameworks/opt/telephony/flags/ims.aconfigH 
�
$com.android.internal.telephony.flagsdo_not_override_precise_label	telephony"TWhen set, Telecom will not override the precise label for certain disconnect causes.*	29696877808B1
+frameworks/opt/telephony/flags/misc.aconfigH
�
$com.android.internal.telephony.flags enable_carrier_config_n1_control	telephony"_enabling this flag allows KEY_CARRIER_NR_AVAILABILITIES_INT_ARRAY to control N1 mode enablement*	30203353508B4
.frameworks/opt/telephony/flags/network.aconfigH 
�
$com.android.internal.telephony.flags log_mms_sms_database_access_info	telephony"VWhether to log MMS/SMS database access info and report anomaly when getting exception.*	27522540208B1
+frameworks/opt/telephony/flags/misc.aconfigH 
�
$com.android.internal.telephony.flagsoem_enabled_satellite_flag	telephony"=This flag controls satellite communication supported by OEMs.*	29181196208B6
0frameworks/opt/telephony/flags/satellite.aconfigH 
�
$com.android.internal.telephony.flagsreject_bad_sub_id_interaction	telephony"yPreviously, the DB allows insertion of a random sub Id, but doesn't allow query it. This change rejects such interaction.*	29412541108B6
0frameworks/opt/telephony/flags/messaging.aconfigH 
�
$com.android.internal.telephony.flagsrelax_ho_teardown	telephony"BRelax handover tear down if the device is currently in voice call.*	27089591208B1
+frameworks/opt/telephony/flags/data.aconfigH 
�
$com.android.internal.telephony.flagssms_domain_selection_enabled	telephony"HThis flag controls AP domain selection support for normal/emergency SMS.*	26280407108B6
0frameworks/opt/telephony/flags/messaging.aconfigH 
�
$com.android.internal.telephony.flagswork_profile_api_split	telephony"nTo support separation between personal and work from TelephonyManager and SubscriptionManager API perspective.*	29607667408B9
3frameworks/opt/telephony/flags/subscription.aconfigH 
�
%com.android.managedprovisioning.flags
cosmic_ray
enterprise"Enables Cosmic Ray features*	29154011108B<
6packages/apps/ManagedProvisioning/aconfig/root.aconfigH 
�
com.android.media.audio.flags$audio_policy_update_mixing_rules_apimedia_audio"O
Enable AudioPolicy.updateMixingRules API for hot-swapping 
audio mixing rules.*	29387452508B;
5frameworks/av/media/audio/aconfig/audio_flags.aconfigH 
�
com.android.media.audio.flagsmutex_priority_inheritancemedia_audio"�
Enable mutex priority inheritance in audioserver 
(std::mutex does not normally transfer priority from 
the blocked thread to the blocking thread).  
This feature helps reduce audio glitching caused by low priority 
blocking threads.*	20949169508B;
5frameworks/av/media/audio/aconfig/audio_flags.aconfigH 
�
com.android.media.flagsDadjust_volume_for_foreground_app_playing_audio_without_media_sessionmedia_solutions"�Gates whether to adjust local stream volume when the app in the foreground is the last app to play audio or adjust the volume of the last active media session that the user interacted with.*	27518543608BR
Lframeworks/base/media/java/android/media/flags/media_better_together.aconfigB�
�vendor/google/release/aconfig/ap11/com.android.media.flags/adjust_volume_for_foreground_app_playing_audio_without_media_session_flag_values.textprotoH 
�
com.android.media.flags%disable_screen_off_broadcast_receivermedia_solutions"NDisables the broadcast receiver that prevents scanning when the screen is off.*	30423462808BR
Lframeworks/base/media/java/android/media/flags/media_better_together.aconfigH 
�
com.android.media.flags5enable_audio_policies_device_and_bluetooth_controllermedia_solutions"MUse Audio Policies implementation for device and Bluetooth route controllers.*	28057622808BR
Lframeworks/base/media/java/android/media/flags/media_better_together.aconfigH 
�
com.android.media.flags%enable_rlp_callbacks_in_media_router2media_solutions"HMake RouteListingPreference getter and callbacks public in MediaRouter2.*	28106710108BR
Lframeworks/base/media/java/android/media/flags/media_better_together.aconfigH 
�
com.android.media.flagsIfallback_to_default_handling_when_media_session_has_fixed_volume_handlingmedia_solutions"�Fallbacks to the default handling for volume adjustment when media session has fixed volume handling and its app is in the foreground and setting a media controller.*	29374397508BR
Lframeworks/base/media/java/android/media/flags/media_better_together.aconfigH 
�
com.android.media.midi.flagsvirtual_umpmedia_audio"Enable virtual UMP MIDI.*	29111517608B:
4frameworks/av/media/audio/aconfig/midi_flags.aconfigH 
�
com.android.net.flagsforbidden_capabilityandroid_core_networking"/This flag controls the forbidden capability API*	30299750508B8
2packages/modules/Connectivity/common/flags.aconfigH 
�
com.android.net.flagsnsd_expired_services_removalandroid_core_networking"-Remove expired services from MdnsServiceCache*	30464938408B8
2packages/modules/Connectivity/common/flags.aconfigH 
�
com.android.net.flags!track_multiple_network_activitiesandroid_core_networking"NNetworkActivityTracker tracks multiple networks including non default networks*	26787018608B8
2packages/modules/Connectivity/common/flags.aconfigH 
|
com.android.nfc.flags	test_flagnfc"Test flag for NFC*	29533905308B/
)packages/apps/Nfc/flags/nfc_flags.aconfigH 
�
com.android.providers.settingssupport_overridescore_experiments_team_internal"DWhen enabled, allows setting and displaying local overrides via adb.*	29839235708Bp
jframeworks/base/packages/SettingsProvider/src/com/android/providers/settings/device_config_service.aconfigH 
�
com.android.sdksandbox.flags"sandbox_activity_sdk_based_contextsdk_sandbox"This flag make the sandbox activity context based on its corresponding SDK package info instead of the sandbox App package info*	29032626708BQ
Kpackages/modules/AdServices/sdksandbox/flags/sandbox_activity_flags.aconfigH 
�
 com.android.server.accessibilityadd_window_token_without_lockaccessibility"ACalls WMS.addWindowToken without holding A11yManagerService#mLock*	29797254808BB
<frameworks/base/services/accessibility/accessibility.aconfigH 
�
 com.android.server.accessibilitydeprecate_package_list_observeraccessibility"/Stops using the deprecated PackageListObserver.*	30456145908BB
<frameworks/base/services/accessibility/accessibility.aconfigH 
�
 com.android.server.accessibility)disable_continuous_shortcut_on_force_stopaccessibility"cWhen a package is force stopped, remove the button shortcuts of any continuously-running shortcuts.*	19801818008BB
<frameworks/base/services/accessibility/accessibility.aconfigH 
�
 com.android.server.accessibilityenable_magnification_joystickaccessibility"5Whether to enable joystick controls for magnification*	29721125708BB
<frameworks/base/services/accessibility/accessibility.aconfigH 
�
 com.android.server.accessibility9enable_magnification_multiple_finger_multiple_tap_gestureaccessibility"BWhether to enable multi-finger-multi-tap gesture for magnification*	25727441108BB
<frameworks/base/services/accessibility/accessibility.aconfigH 
�
 com.android.server.accessibilitypinch_zoom_zero_min_spanaccessibility"8Whether to set min span of ScaleGestureDetector to zero.*	29532779208BB
<frameworks/base/services/accessibility/accessibility.aconfigH 
�
 com.android.server.accessibility)proxy_use_apps_on_virtual_device_listeneraccessibility"-Fixes race condition described in b/286587811*	28658781108BB
<frameworks/base/services/accessibility/accessibility.aconfigH 
�
 com.android.server.accessibility$reduce_touch_exploration_sensitivityaccessibility"�Reduces touch exploration sensitivity by only sending a hover event when the ifnger has moved the amount of pixels defined by the system's touch slop.*	30367786008BB
<frameworks/base/services/accessibility/accessibility.aconfigH 
�
 com.android.server.accessibilityscan_packages_without_lockaccessibility"VScans packages for accessibility service/activity info without holding the A11yMS lock*	29596987308BB
<frameworks/base/services/accessibility/accessibility.aconfigH 
�
 com.android.server.accessibilitysend_a11y_events_based_on_stateaccessibility"�Sends accessibility events in TouchExplorer#onAccessibilityEvent based on internal state to keep it consistent. This reduces test flakiness.*	29557568408BB
<frameworks/base/services/accessibility/accessibility.aconfigH 
�
$com.android.server.companion.virtualdump_historyvirtual_devices"UThis flag controls if a history of virtual devices is shown in dumpsys virtualdevices*	29311471908B`
Zframeworks/base/services/companion/java/com/android/server/companion/virtual/flags.aconfigH 
�
(com.android.server.display.feature.flags2back_up_smooth_display_and_force_peak_refresh_ratedisplay_manager"FFeature flag for backing up Smooth Display and Force Peak Refresh Rate*	21173758808Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flags#enable_adaptive_tone_improvements_1display_manager"+Feature flag for Adaptive Tone Improvements*	29955075508Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flags#enable_adaptive_tone_improvements_2display_manager"3Feature flag for Further Adaptive Tone Improvements*	29476263208Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flags'enable_connected_display_error_handlingdisplay_manager"1Feature flag for connected display error handling*	28346147208Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flags#enable_connected_display_managementdisplay_manager"-Feature flag for Connected Display management*	28073950808Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flagsenable_display_offloaddisplay_manager"Feature flag for DisplayOffload*	29952164708Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flags&enable_display_resolution_range_votingdisplay_manager"7Feature flag to enable voting for ranges of resolutions*	29929705808Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flags-enable_displays_refresh_rates_synchronizationdisplay_manager"8Enables synchronization of refresh rates across displays*	29401584508Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH 
�
(com.android.server.display.feature.flagsenable_hdr_clamperdisplay_manager"Feature flag for HDR Clamper*	29510004308Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flags&enable_mode_limit_for_external_displaydisplay_manager"=Feature limiting external display resolution and refresh rate*	24209354708Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flagsenable_nbm_controllerdisplay_manager"2Feature flag for Normal Brightness Mode Controller*	29952754908Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flagsenable_power_throttling_clamperdisplay_manager")Feature flag for Power Throttling Clamper*	29477700708Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flagsenable_small_area_detectiondisplay_manager"#Feature flag for SmallAreaDetection*	29872218908Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
(com.android.server.display.feature.flagsenable_user_preferred_mode_votedisplay_manager"<Feature flag to use voting for UserPreferredMode for display*	29701861208Ba
[frameworks/base/services/core/java/com/android/server/display/feature/display_flags.aconfigH
�
com.android.server.job6relax_prefetch_connectivity_constraint_only_on_chargerbackstage_power"OOnly relax a prefetch job's connectivity constraint when the device is charging*	29932994808BC
=frameworks/base/apex/jobscheduler/service/aconfig/job.aconfigH 
�
 com.android.server.telecom.flags;available_routes_never_updated_after_set_system_audio_statetelecom"5Fix supported routes wrongly include bluetooth issue.*	29259975108BW
Qpackages/services/Telecomm/flags/telecom_callaudioroutestatemachine_flags.aconfigH 
�
 com.android.server.telecom.flagscall_details_id_changestelecom"RWhen set, call details/extras id updates to Telecom APIs for Android V are active.*	30171356008B@
:packages/services/Telecomm/flags/telecom_api_flags.aconfigH 
�
 com.android.server.telecom.flagsearly_binding_to_incall_servicetelecom"EBinds to InCallServices when call requires no call filtering on watch*	28211326108BJ
Dpackages/services/Telecomm/flags/telecom_incallservice_flags.aconfigH 
�
 com.android.server.telecom.flags)is_new_outgoing_call_broadcast_unblockingtelecom"?When set, the ACTION_NEW_OUTGOING_CALL broadcast is unblocking.*	22455086408BF
@packages/services/Telecomm/flags/telecom_broadcast_flags.aconfigH 
�
 com.android.server.telecom.flags&only_update_telephony_on_valid_sub_idstelecom"VFor testing purposes, only update Telephony when the default calling subId is non-zero*	23484628208BR
Lpackages/services/Telecomm/flags/telecom_default_phone_account_flags.aconfigH 
�
 com.android.server.telecom.flags,skip_filter_phone_account_perform_dnd_filtertelecom"XGates whether to still perform Dnd filter when phone account has skip_filter call extra.*	22233386908BK
Epackages/services/Telecomm/flags/telecom_call_filtering_flags.aconfigH 
�
 com.android.server.telecom.flags#telecom_log_external_wearable_callstelecom"5log external call if current device is a wearable one*	29260075108BD
>packages/services/Telecomm/flags/telecom_calllog_flags.aconfigH 
�
 com.android.server.telecom.flags#telecom_resolve_hidden_dependenciesandroid_platform_telecom"(Mainland cleanup for hidden dependencies*b/30344037008BR
Lpackages/services/Telecomm/flags/telecom_resolve_hidden_dependencies.aconfigH 
�
 com.android.server.telecom.flags*telephony_has_default_but_telecom_does_nottelecom"�Telecom is requesting the user to select a sim account to place the outgoing call on but the user has a default account in the settings*	30239709408BR
Lpackages/services/Telecomm/flags/telecom_default_phone_account_flags.aconfigH 
�
 com.android.server.telecom.flags/use_device_provided_serialized_ringer_vibrationtelecom"BGates whether to use a serialized, device-specific ring vibration.*	28211326108BO
Ipackages/services/Telecomm/flags/telecom_ringer_flag_declarations.aconfigH 
�
 com.android.server.telecom.flagsvoip_app_actions_supporttelecom"LWhen set, Telecom support for additional VOIP application actions is active.*	29693427808B@
:packages/services/Telecomm/flags/telecom_api_flags.aconfigH 
�
#com.android.settingslib.media.flagsenable_tv_media_output_dialogtv_system_ui"=Gates all the changes for the tv specific media output dialog*	30320563108B^
Xframeworks/base/packages/SettingsLib/aconfig/settingslib_media_flag_declarations.aconfigH 
�
#com.android.settingslib.media.flags(use_media_router2_for_info_media_managermedia_solutions"�Gates whether to use a MediaRouter2-based implementation of InfoMediaManager, instead of the legacy MediaRouter2Manager-based implementation.*	19265781208B^
Xframeworks/base/packages/SettingsLib/aconfig/settingslib_media_flag_declarations.aconfigH 
�
com.android.text.flagscustom_locale_fallbacktext"�A feature flag that adds custom locale fallback to the vendor customization XML. This enables vendors to add their locale specific fonts, e.g. Japanese font.*	27876895808BQ
Kframeworks/base/core/java/android/text/flags/custom_locale_fallback.aconfigH
�
com.android.text.flagsdeprecate_fonts_xmltext"�Feature flag for deprecating fonts.xml. By setting true for this feature flag, the new font configuration XML, /system/etc/font_fallback.xml is used. The new XML has a new syntax and flexibility of variable font declarations, but it is not compatible with the apps that reads fonts.xml. So, fonts.xml is maintained as a subset of the font_fallback.xml*	28176962008BN
Hframeworks/base/core/java/android/text/flags/deprecate_fonts_xml.aconfigH
�
com.android.text.flagsfix_double_underlinetext"^Feature flag for fixing double underline because of the multiple font used in the single line.*	29733672408BO
Iframeworks/base/core/java/android/text/flags/fix_double_underline.aconfigH 
�
com.android.text.flagsfix_line_height_for_localetext"vFeature flag that preserve the line height of the TextView and EditText even if the the locale is different from Latin*	30332670808BU
Oframeworks/base/core/java/android/text/flags/fix_line_height_for_locale.aconfigH 
�
com.android.text.flagsno_break_no_hyphenation_spantext"QA feature flag that adding new spans that prevents line breaking and hyphenation.*	28319358608BW
Qframeworks/base/core/java/android/text/flags/no_break_no_hyphenation_span.aconfigH 
�
com.android.text.flagsphrase_strict_fallbacktext"VFeature flag for automatic fallback from phrase based line break to strict line break.*	28197087508BQ
Kframeworks/base/core/java/android/text/flags/phrase_strict_fallback.aconfigH 
�
com.android.text.flagsuse_bounds_for_widthtext"0Feature flag for preventing horizontal clipping.*6393820608BO
Iframeworks/base/core/java/android/text/flags/use_bounds_for_width.aconfigH 
�
com.android.text.flags#use_optimized_boottime_font_loadingtext"BFeature flag ensuring that font is loaded once and asynchronously.*	30440688808BQ
Kframeworks/base/core/java/android/text/flags/optimized_font_loading.aconfigH
�
com.android.window.flags,activity_embedding_overlay_presentation_flagwindowing_sdk"3Whether the overlay presentation feature is enabled*	24351873808BJ
Dframeworks/base/core/java/android/window/flags/windowing_sdk.aconfigH 
�
com.android.window.flags*close_to_square_config_includes_status_barwindowing_frontend"MOn close to square display, when necessary, configuration includes status bar*	29187075608BO
Iframeworks/base/core/java/android/window/flags/windowing_frontend.aconfigH 
�
com.android.window.flagsdimmer_refactorwindowing_frontend"Refactor dim to fix flickers*	29529101908BO
Iframeworks/base/core/java/android/window/flags/windowing_frontend.aconfigH
�
com.android.window.flagsexplicit_refresh_rate_hintswindow_surfaces",Performance related hints during transitions*	30001913108BL
Fframeworks/base/core/java/android/window/flags/window_surfaces.aconfigH
�
com.android.window.flagsnav_bar_transparent_by_defaultwindowing_frontend"JMake nav bar color transparent by default when targeting SDK 35 or greater*	23219550108BO
Iframeworks/base/core/java/android/window/flags/windowing_frontend.aconfigH 
�
com.android.window.flagssurface_trusted_overlaywindow_surfaces"LWhether to add trusted overlay flag on the SurfaceControl or the InputWindow*	29203292608BL
Fframeworks/base/core/java/android/window/flags/window_surfaces.aconfigH
�
com.android.window.flagssync_window_config_update_flagwindowing_sdk"NWhether the feature to sync different window-related config updates is enabled*	26087352908BJ
Dframeworks/base/core/java/android/window/flags/windowing_sdk.aconfigH 
�
com.android.window.flags#task_fragment_system_organizer_flagwindowing_sdk"<Whether the TaskFragment system organizer feature is enabled*	28405004108BJ
Dframeworks/base/core/java/android/window/flags/windowing_sdk.aconfigH 
�
com.android.window.flagstransit_ready_trackingwindowing_frontend"-Enable accurate transition readiness tracking*	29492549808BO
Iframeworks/base/core/java/android/window/flags/windowing_frontend.aconfigH 
�
com.android.window.flagswallpaper_offset_asyncwindowing_frontend"'Do not synchronise the wallpaper offset*	29324875408BO
Iframeworks/base/core/java/android/window/flags/windowing_frontend.aconfigH
�
com.android.window.flagswindow_state_resize_item_flagwindowing_sdk"FWhether to dispatch window resize through ClientTransaction is enabled*	30187095508BJ
Dframeworks/base/core/java/android/window/flags/windowing_sdk.aconfigH 